import React from 'react';
import m1 from './Menu.module.scss';

const Menu = (props) => {
  return (
    <div className={m1.Menu}>
      <div className={m1.details + ' ' + m1.row}>
        <div className={m1.content}>
          <img src="" alt="leaf" />
          <span>TOTAL: 450 KG</span>
          <button>DETAILS</button>
        </div>
      </div>
      <div className={m1.links}>
        <a href="/" className="limk">Home</a>
        <a href="/about" className="limk">About us</a>
        <a href="/contacts" className="limk">Contacts</a>
        <a href="/chackout" className="limk">Chackout</a>
      </div>


    </div>
  )
}

export default Menu;
