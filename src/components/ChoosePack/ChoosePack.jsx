import React from 'react';
import c1 from './WChoosePackmodule.css';

const ChoosePack = (props) => {
  return (
    <div className={c1.ChoosePack}>
      SMALL PACK(100KG)
      MEDIUM PACK(150KG)
      LARGE PACK(200KG)
    </div>
  )
}

export default ChoosePack;
