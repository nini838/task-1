import { configureStore } from '@reduxjs/toolkit'
import counterReducer from '../components/Footer/counterSlice'

export const store = configureStore({
  reducer: {
    counter: counterReducer,
  },
})
