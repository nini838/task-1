import Footer from './components/Footer/Footer';
import Information from './components/Information/Information.jsx';
import Menu from './components/Menu/Menu.jsx';
import Workspace from './components/Workspace/Workspace.jsx';

import './App.scss';

function App() {
  return (
    <div className="App">
      <Menu/>
      <Workspace/>
      <Information/>
      <Footer/>
    </div>
  );
}

export default App;
